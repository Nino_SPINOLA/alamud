# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import SneakEvent

class SneakAction(Action2):
    EVENT = TypeEvent
    ACTION = "sneak"
    RESOLVE_OBJECT = "resolve_for_operate"